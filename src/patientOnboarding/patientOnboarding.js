import { Button, Grid, makeStyles, Typography } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import PatientCard from '../components/PatientCard';
import CareAPI, { setClientToken } from '../common/CareAPI';
import IconButtonsComponent from '../components/IconButtonsComponent';
import SearchBar from "material-ui-search-bar";
import CustomIcons from '../components/CustomIcons';
import { useHistory } from 'react-router';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1
    },
    paper: {
        padding: theme.spacing(1),
        textAlign: 'left',
        color: theme.palette.text.secondary
    },
    reffer: {
        fontWeight: 'bold',
        fontSize: '24px',
        marginBottom: '24px'
    },
    Typography: {
        marginTop: '14px',
        fontWeight: 'bold'
    }
}))


const Patientonboarding = () => {
    const [patientDetails, setPatientDetails] = useState([])
    const [state, setState] = useState({value: null})
    const classes = useStyles();
    const [data, setData] = useState({})
    const history = useHistory()

    useEffect(() => {
        if (localStorage && localStorage.getItem('access_token')) {
            setClientToken(localStorage.getItem('access_token'));
        }

        getPatientDetails();
    }, [])

    const getPatientDetails = async () => {
        try {
            const res = await CareAPI.get('/api/patients');
           
            if (res && res.status === 200) {
                setData(res?.data?.statusInfo)
                return setPatientDetails(res?.data?.patientList)
            }
        } catch (error) {
            console.log(error);
        }
    }

    const doSomethingWith = (value) => {
        if(value) {
            console.log(value)
        }
        
    }

    const Header = () => (
        <>
            <Grid container>
                <Grid item spacing={1} sm={4} xs={12}>
                    <Typography className={classes.Typography}>Click on the patient card to assign the bed</Typography>
                </Grid>
                <Grid item spacing={1} sm={3} xs={12}>
                    <Grid container>
                        <Grid item spacing={2} md={2}><Typography>{data?.emergency}</Typography> <CustomIcons style={{color: '#a30f0f'}}/></Grid>
                        <Grid item spacing={2} md={2}><Typography>{data?.moderate}</Typography> <CustomIcons style={{color: '#dbaa09'}}/></Grid>
                        <Grid item spacing={2} md={2}><Typography>{data?.normal < 9 ? '0'+data?.normal : data.normal}</Typography> <CustomIcons style={{color: 'green'}}/></Grid>
                    </Grid>
                </Grid>
                <Grid item spacing={1} sm={2} xs={12}>
                    <IconButtonsComponent onClick={() => history.push('/home/add-patient')}/>
                </Grid>
                <Grid item spacing={1} sm={2} xs={12}>
                    <SearchBar
                        // value={state.value}
                        // onChange={(newValue) => setState({ value: newValue })}
                        // onRequestSearch={() => doSomethingWith(state.value)}
                    />
                </Grid>
                <Grid item spacing={1} sm={2} xs={12}></Grid>
            </Grid>
        </>
    )

    
    return (
        <>
            <div className={classes.root}>
                <Grid container spacing={1}>
                    <Grid container spacing={1}>
                        <Typography className={classes.reffer}>Refer / Add Patient Details</Typography>
                    </Grid>
                    <Grid container spacing={1} style={{ marginBottom: '24px' }}>
                        <Header />
                    </Grid>
                    <Grid container spacing={2}>
                        {patientDetails && patientDetails.map((res, index) => (
                            <div key={index}>
                                <PatientCard data={res} />
                            </div>
                        ))}

                    </Grid>
                </Grid>
            </div>
        </>
    );
}

export default Patientonboarding;