import _ from 'lodash';

// Methods
export const getServerBaseUrl = () => {
    let baseUrl = window.location.origin;
    console.log(baseUrl);

    let devHost = ['3.108.138.223'];
    let hosts = ['localhost', '127.0.0.1'];
    if (_.some(hosts, (el) => _.includes(baseUrl, el))) {
        baseUrl = "https://care4covid.in";
        //baseUrl = "http://localhost:3070";
    } else if(_.some(devHost, (el) => _.includes(baseUrl, el))) {
        baseUrl = "http://3.108.138.223:8080";
    }
    
    console.log(baseUrl);
    return baseUrl;
};

