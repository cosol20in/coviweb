import axios from 'axios';
import {getServerBaseUrl} from './utils';

const CareAPI = axios.create({
  baseURL: getServerBaseUrl()
});

// Set JSON Web Token in Client to be included in all calls
export const setClientToken = token => {
  CareAPI.interceptors.request.use(function(config) {
    const tokens = localStorage.getItem('access_token')
    config.headers.Authorization = `Bearer ${tokens}`;
    return config;
  });
};


export default CareAPI;
