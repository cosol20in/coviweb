
import { Button, Card, CardContent, Grid, makeStyles, SvgIcon, Typography } from '@material-ui/core';
import React from 'react';
import CustomIcons from './CustomIcons';
import Icon from '@material-ui/core/Icon';
import { ReactComponent as cardiogram } from '../assets/icons/cardiogram.svg'
import { useHistory } from 'react-router';
import OpenWithIcon from '@material-ui/icons/OpenWith';
import Expand from '../assets/icons/expand.png'

const useStyles = makeStyles((theme) => ({
    root: {
        minWidth: 330,
        maxWidth: 330,
        minHeight: 220,
        //maxHeight: 200,
        margin: 8,
        background: '#f5f5f5'
    },
    patientStyle: {
        fontWeight: 'bold',
        paddingLeft: '3px',
        whiteSpace: 'nowrap',
        width: 220,
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        lineHeight: '22px'
    }
}))

const PatientCard = (props) => {
    const classes = useStyles();
    const history = useHistory();

    const onClickBed = () => {
        history.push({
            pathname: '/home/patient-bed-management',
            state: {details: props}
        })
    }

    const onClickPatientDetails = () => {
        history.push({
            pathname: '/home/patient-details',
            state: {details: props}
        })
    }

    return (
        <>
            <Card className={classes.root}>
                <CardContent>
                    <Grid container>
                        <Grid item >
                            <Grid container>
                                <Grid container xs={10}>
                                    <Grid item><Typography variant='body2'>Patient Name: </Typography></Grid>
                                    <Grid item ><Typography title={`${props?.data?.firstName} ${props?.data?.lastName}`} className={classes.patientStyle} variant='body2' style={{width: '140px'}}>{props?.data?.firstName} {props?.data?.lastName}</Typography></Grid>
                                </Grid>
                                <Grid container xs={1}>
                                    {props.data.status && props.data.status === 'emergency' && <CustomIcons style={{color: '#a30f0f'}}/>}
                                    {props.data.status && props.data.status === 'moderate' && <CustomIcons style={{color: '#dbaa09'}}/>}
                                    {props.data.status && props.data.status === 'normal' && <CustomIcons style={{color: 'green'}}/>}
                                </Grid>
                                <Grid xs={1} style={{paddingLeft: '20px'}}>
                                    <img onClick={onClickPatientDetails} src="https://img.icons8.com/material/24/000000/expand--v3.png" alt='expand' width='16px' height='16px' style={{cursor: 'pointer'}}/>
                                </Grid>

                            </Grid>
                            
                            <Grid container>
                                <Grid item><Typography variant='body2'>Age:</Typography></Grid>
                                <Grid item ><Typography variant='body2' className={classes.patientStyle}>{props?.data?.age}</Typography></Grid>
                            </Grid>
                            <Grid container>
                                <Grid item><Typography variant='body2'>Gender:</Typography></Grid>
                                <Grid item ><Typography variant='body2' className={classes.patientStyle}>{props?.data?.gender}</Typography></Grid>
                            </Grid>
                            <Grid container>
                                <Grid item><Typography variant='body2'>Location: </Typography></Grid>
                                <Grid item >
                                    <Typography variant='body2' title={`${props?.data?.address?.street}, ${props?.data?.address?.city} - ${props?.data?.address?.pincode}`} className={classes.patientStyle}>{props?.data?.address?.street}, {props?.data?.address?.city} - {props?.data?.address?.pincode}</Typography>
                                </Grid>
                            </Grid>
                            <Grid container>
                                <Grid item><Typography variant='body2'>Status: </Typography></Grid>
                                <Grid item >
                                    <Typography variant='body2' title={`${props?.data?.address?.status}`} className={classes.patientStyle}>{props?.data?.bedStatus}</Typography>
                                </Grid>
                            </Grid>
                        </Grid>

                        
                    </Grid>
                    <Grid container style={{justifyContent: 'center', paddingTop: '36px'}}>
                        
                        <Button color='primary' variant='outlined' disabled={props?.data?.bedStatus === 'reserved'} onClick={onClickBed}>Assign Bed</Button>
                    </Grid>
                </CardContent>
            </Card>
        </>
    );
}

export default PatientCard;