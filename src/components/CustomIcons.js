
import { SvgIcon } from '@material-ui/core';
import React from 'react';
import { ReactComponent as cardiogram } from '../assets/icons/cardiogram.svg'

const CustomIcons = (props) => (
    <SvgIcon component={cardiogram}  viewBox="0 0 600 476.6" {...props}></SvgIcon>
)


export default CustomIcons;