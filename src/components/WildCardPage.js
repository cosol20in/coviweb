
import { Typography } from '@material-ui/core';
import React from 'react';

const WildCardPage = () => {
    return ( 
        <div>
            <Typography variant='h3' align='center'>Page Not Found. 404!</Typography>
        </div>
     );
}
 
export default WildCardPage;