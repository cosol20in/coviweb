
import { Button, makeStyles, SvgIcon } from '@material-ui/core';
import React from 'react';
import AddCircleIcon from '@material-ui/icons/AddCircle';

const useStyles = makeStyles((theme) => (
    {
        root: {
            margin: theme.spacing(1)
        }
    }
))

const IconButtonsComponent = (props) => {
    const classes = useStyles();
    return (
        <>
            <Button {...props} variant="contained"
                color='primary'
                size='large'
                className={classes.root}
                startIcon={<AddCircleIcon />}>
                Add Patient

            </Button>
        </>
    )
}

export default IconButtonsComponent;