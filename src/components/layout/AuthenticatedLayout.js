import React from 'react';
import Header from './Header';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import Container from '@material-ui/core/Container';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerContainer: {
    overflow: 'auto',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));

const AuthenticatedLayout = (props) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      {/* <Header /> */}
      <Drawer
        className={classes.drawer}
        variant="temporary"
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <Toolbar />
        <div className={classes.drawerContainer}>
          <List>
            <ListItem button key={'Menu 1'}>
              <ListItemIcon><InboxIcon /></ListItemIcon>
              <ListItemText primary={'Menu 1'} />
            </ListItem>
            <ListItem button key={'Menu 2'}>
              <ListItemIcon><InboxIcon /></ListItemIcon>
              <ListItemText primary={'Menu 2'} />
            </ListItem>
          </List>
        </div>
      </Drawer>
      <Container maxWidth={false}>
        <Toolbar />
        {props.children}
      </Container>
    </div>
  )
}

export default AuthenticatedLayout;