import React, { useState, useEffect } from 'react';
import AuthenticatedLayout from './AuthenticatedLayout';
import AnonymousLayout from './AnonymousLayout';
import {setClientToken} from '../../common/CareAPI'

const Layout = (props) => {
  const [authenticated, setAuthenticated] = useState(false);

  useEffect(() => {
    if (localStorage && localStorage.getItem('access_token')) {
      setClientToken(localStorage.getItem('access_token'));
      setAuthenticated(localStorage.getItem('access_token'));
    }
  }, [])
  return (
    <>
      {authenticated ? (
        <AuthenticatedLayout>
          {props.children}
        </AuthenticatedLayout>
      ) : (
        <AnonymousLayout>
          {props.children}
        </AnonymousLayout>
      )}
    </>
  )
}

export default Layout;