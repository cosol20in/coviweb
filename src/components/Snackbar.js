
import { makeStyles, Snackbar } from '@material-ui/core';
import React, { useState } from 'react';
import Alert from '@material-ui/lab/Alert';

const useStyles = makeStyles((theme) => (
    {
        root: {
            width: '100%',
            '& > * + *': {
                marginTop: theme.spacing(2),
            },
        }
    }
))


const SnackBar = ({open, type, message}) => {
    const classes = useStyles();
    // const [open, setOpen] = useState(false);

    // const handleOnClose = (event, reason) => {
    //     if (reason === 'clickaway') {
    //         return;
    //     }

    //     setOpen(true)

    // }

    return (
        <div className={classes.root}>
            <Snackbar open={open} autoHideDuration={5000} >
                <Alert  severity={type} variant='filled'>
                    {message}
                </Alert>
            </Snackbar>

        </div>
    );
}

export default SnackBar;