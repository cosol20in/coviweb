import Help from './Help';
import Faq from './Faq';
import BedManagement from './bedManagement/BedManagement';
import Login from './Login';
import Layout from './components/layout/Layout';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import './App.scss';
import Dashboard from './dashboard/dashboard';
import patientBedManagement from './PatientBedManagement/PatientBedManagement';
import PatientBedManagement from './PatientBedManagement/PatientBedManagement';
import SnackBar from './components/Snackbar';


// Need to refactore this code with HOCs
function App() {

  return (
    <div>
      <Router>


        <Switch>
          <Route exact path='/'>
            <Login />
          </Route>
          {localStorage && localStorage.getItem('auth') && (
            <Route path='/home'>
              <Dashboard />
            </Route>
          )}
          <Route path="/help">
            <Help />
          </Route>
          <Route path="/faq">
            <Faq />
          </Route>
        </Switch>

      </Router >


    </div>
  );
}

export default App;
