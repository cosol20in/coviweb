import React from "react";
import { Button, Grid, Box, Divider, Typography } from "@material-ui/core";
import { useHistory, useLocation } from "react-router-dom";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import { makeStyles, useTheme } from "@material-ui/core/styles";

import * as moment from "moment";

const useStyles = makeStyles((theme) => ({
  text: {
    fontWeight: "bold",
  },
  dot: {
    height: "16px",
    width: "16px",
    backgroundColor: "green",
    borderRadius: "55%",
    display: "inline-block",
  },
  vir: {
    borderLeft: "2px solid rgb(219, 170, 9)",
    height: "60px",
    //position: "absolute",
    marginLeft: "7px",
    marginTop: "-26px",
  },
  status: {
    marginLeft: "8px",
    //fontWeight: 'bold'
  },
  date: {
    marginLeft: "28px",
    fontSize: "10px",
  },
}));

const PatientDetails = () => {
  const history = useHistory();
  const location = useLocation();

  const { data } = location?.state?.details;
  console.log(data);

  const classes = useStyles();

  return (
    <>
      <Button
        onClick={() => history.push("/home")}
        startIcon={<ArrowBackIosIcon />}
      >
        Back
      </Button>
      <Box pb={4} pt={3}>
        <Typography variant="h5" align="center">
          Patient Details
        </Typography>
      </Box>

      <Box>
        <Grid container>
          <Grid item sm={12} xs={12}>
            <Box p={2}>
              Patient Name:{" "}
              <span className={classes.text}>
                {data?.firstName.toUpperCase()} {data?.lastName.toUpperCase()}
              </span>
            </Box>
          </Grid>
        </Grid>
        <Grid container>
          <Grid item sm={4}>
            <Box p={2}>
              Age: <span className={classes.text}>{data?.age}</span>
            </Box>
          </Grid>
          <Grid item sm={4}>
            <Box p={2}>
              Location:{" "}
              <span className={classes.text}>
                {data?.address?.street?.toUpperCase()} -{" "}
                {data?.address?.city?.toUpperCase()} - {data?.address?.pincode}
              </span>
            </Box>
          </Grid>
          <Grid item sm={4}>
            <Box p={2}>
              Suggested Bed:{" "}
              <span className={classes.text}>
                {data?.moreInfo?.requested_bedtype?.toUpperCase()}
              </span>
            </Box>
          </Grid>
        </Grid>
        <Grid container>
          <Grid item sm={12} xs={12}>
            <Box p={2}>
              Gender:{" "}
              <span className={classes.text}>
                {data?.gender?.toUpperCase()}
              </span>
            </Box>
          </Grid>
        </Grid>
        <Divider />
      </Box>
      <Box p={3}>
        <Grid container direction="row">
          <Grid direction="column" sm={4}>
            <Box pt={2}>
              <Grid container>
                <Grid item sm={6}>
                  Patient Adhaar Number:
                </Grid>
                <Grid item sm={6} className={classes.text}>
                  {data?.identity?.adhaar}
                </Grid>
              </Grid>
            </Box>
            <Box pt={2}>
              <Grid container>
                <Grid item sm={6}>
                  Patient Mobile Number:
                </Grid>
                <Grid item sm={6} className={classes.text}>
                  {data.phones[0]}
                </Grid>
              </Grid>
            </Box>
            <Box pt={2}>
              <Grid container>
                <Grid item sm={6}>
                  Email:
                </Grid>
                <Grid item sm={6} className={classes.text}></Grid>
              </Grid>
            </Box>
            <Box pt={2}>
              <Grid container>
                <Grid item sm={6}>
                  Symptoms:
                </Grid>
                <Grid item sm={6} className={classes.text}>
                  {data?.moreInfo?.symptoms?.toUpperCase()}
                </Grid>
              </Grid>
            </Box>
            <Box pt={2}>
              <Grid container>
                <Grid item sm={6}>
                  SPO2 (Oxygen %):
                </Grid>
                <Grid item sm={6} className={classes.text}>
                  {data?.moreInfo?.oxygen}
                </Grid>
              </Grid>
            </Box>
            <Box pt={2}>
              <Grid container>
                <Grid item sm={6}>
                  Patient is in:
                </Grid>
                <Grid item sm={6} className={classes.text}>
                  {data?.moreInfo?.patientIsIn?.toUpperCase()}
                </Grid>
              </Grid>
            </Box>
            <Box pt={2}>
              <Grid container>
                <Grid item sm={6}>
                  Name of Hospital / PHC:
                </Grid>
                <Grid item sm={6} className={classes.text}>
                  {data?.moreInfo?.hospital?.toUpperCase()}
                </Grid>
              </Grid>
            </Box>
            <Box pt={2}>
              <Grid container>
                <Grid item sm={6}>
                  Ward Number / Name:
                </Grid>
                <Grid item sm={6} className={classes.text}>
                  {data?.moreInfo?.ward?.toUpperCase()}
                </Grid>
              </Grid>
            </Box>
          </Grid>
          <Grid direction="column" sm={4}>
            <Box pt={2}>
              <Grid container>
                <Grid item sm={6}>
                  Covid Test:
                </Grid>
                <Grid item sm={6} className={classes.text}>
                  {data?.reports?.covidTest?.toUpperCase()}
                </Grid>
              </Grid>
            </Box>
            <Box pt={2}>
              <Grid container>
                <Grid item sm={6}>
                  SRF Number / BU Number:
                </Grid>
                <Grid item sm={6} className={classes.text}>
                  {data?.moreInfo?.srf?.toUpperCase()}
                </Grid>
              </Grid>
            </Box>
            <Box pt={2}>
              <Grid container>
                <Grid item sm={6}>
                  COVID Result:
                </Grid>
                <Grid item sm={6} className={classes.text}>
                  {data?.reports?.covidResult?.toUpperCase()}
                </Grid>
              </Grid>
            </Box>
            <Box pt={2}>
              <Grid container>
                <Grid item sm={6}>
                  Attendant Name:
                </Grid>
                <Grid item sm={6} className={classes.text}>
                  {data?.moreInfo?.attendantName?.toUpperCase()}
                </Grid>
              </Grid>
            </Box>
            <Box pt={2}>
              <Grid container>
                <Grid item sm={6}>
                  Attendant Mobile Number:
                </Grid>
                <Grid item sm={6} className={classes.text}>
                  {data?.moreInfo?.attendantMobile?.toUpperCase()}
                </Grid>
              </Grid>
            </Box>
            <Box pt={2}>
              <Grid container>
                <Grid item sm={6}>
                  comorbidities:
                </Grid>
                <Grid item sm={6} className={classes.text}>
                  {data?.moreInfo?.comorbidities?.toUpperCase()}
                </Grid>
              </Grid>
            </Box>
            <Box pt={2}>
              <Grid container>
                <Grid item sm={6}>
                  Prefeered Hospital:
                </Grid>
                <Grid item sm={6} className={classes.text}></Grid>
              </Grid>
            </Box>

            <Box pt={2}>
              <Grid container>
                <Grid item sm={6}>
                  Others:
                </Grid>
                <Grid item sm={6} className={classes.text}>
                  {data?.moreInfo?.others?.toUpperCase()}
                </Grid>
              </Grid>
            </Box>
          </Grid>
          <Grid direction="column" sm={4}>
            <Box p={2}>
              <Typography variant='subtitle1' style={{fontWeight: 'bold'}}>History</Typography>
            </Box>
            <Box p={2} pl={4} pt={0}>
              {data?.history?.map((item, index) => (
                <div key={index}>
                  <div className={classes.dot}></div>{" "}
                  <span className={classes.status}> {item?.content}</span>{" "}
                  <div className={classes.date}>
                    {moment(item?.dt).format("MMMM DD YYYY, h:mm:ss a")}
                  </div>
                  <div className={classes.vir}></div>
                </div>
              ))}
            </Box>
          </Grid>
        </Grid>
      </Box>
    </>
  );
};

export default PatientDetails;
