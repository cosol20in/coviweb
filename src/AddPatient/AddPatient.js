import React, { useState } from "react";
import { Grid } from "@material-ui/core";
import { ErrorMessage, Field, Form, Formik } from "formik";
import * as Yup from "yup";
import CareAPI from "../common/CareAPI";
import { useSnackbar } from "notistack";
import { useHistory } from "react-router-dom";

const AddPatient = () => {
  const [open, setOpen] = useState(false);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const history = useHistory();

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  const postPatient = async (getFields, resetForm) => {
    const intialState = {
      firstName: getFields.firstName,
      lastName: getFields.lastName,
      age: getFields.age,
      gender: getFields.gender,
      identity: {
        adhaar: getFields.adhaar,
      },
      address: {
        street: getFields.address,
        city: getFields.city,
        pincode: getFields.pincode,
      },
      phones: [getFields.mobile],
      reports: {
        covidTest: getFields.covidTest,
        covidResult: getFields.covidResult,
      },
      status: "emergency",
      moreInfo: {
        symptoms: getFields.symptoms,
        oxygen: getFields.oxygen,
        patientIsIn: getFields.patientIsIn,
        hospital: getFields.hospital,
        ward: getFields.ward,
        srf: getFields.srf,
        attendantName: getFields.attendantName,
        attendantMobile: getFields.attendantMobile,
        comorbidities: getFields.comorbidities,
        hospital_type: getFields.preferredHospital,
        requested_bedtype: getFields.bedTypes,
        others: getFields.others,
      },
    };

    try {
      const res = await CareAPI.post("/api/patients", intialState);
      if (res && res.status === 200) {
        resetForm();
        enqueueSnackbar("Patient is added successfuly", { variant: "success" });
        history.push('/home')
      }
    } catch (error) {
      enqueueSnackbar("Some issue there to adding the patients", {
        variant: "error",
      });
    }
  };

  return (
    <div style={{ paddingLeft: "3%", paddingRight: "3%" }}>
      <h5 style={{ marginBottom: "36px" }}>
        Please enter patient details for onboarding
      </h5>

      <Formik
        initialValues={{
          firstName: "",
          lastName: "",
          age: "",
          gender: "male",
          adhaar: "",
          mobile: "",
          email: "",
          address: "",
          city: "",
          pincode: "",
          symptoms: "",
          oxygen: "",
          patientIsIn: "home",
          hospital: "",
          ward: "",
          covidTest: "rtpcr",
          srf: "",
          covidResult: "positive",
          attendantName: "",
          attendantMobile: "",
          comorbidities: "",
          preferredHospital: "govt",
          bedTypes: "icu",
          others: "",
        }}
        validationSchema={Yup.object().shape({
          firstName: Yup.string().required("First Name is required"),
          lastName: Yup.string().required("Last Name is required"),
          age: Yup.string().required("Age is required"),
          // gender: Yup.string()
          //     .required('Gender is required'),
          // adhaar: Yup.string()
          //     .required('Adhaar is required'),
          mobile: Yup.string().required("Mobile is required"),
          // email: Yup.string()
          //     .email('Email is invalid')
          //     .required('Email is required'),
          address: Yup.string().required("Address is required"),
          // city: Yup.string()
          //     .required('City is required'),
          // pincode: Yup.string()
          //     .required('Pincode is required'),
          // symptoms: Yup.string()
          //     .required('Symptoms is required'),
          // oxygen: Yup.string()
          //     .required('Oxygen % is required'),
          // patientIsIn: Yup.string()
          //     .required('Pateient IS IN is required'),
          // hospital: Yup.string()
          //     .required('Name of Hospital is required'),
          // ward: Yup.string()
          //     .required('Ward is required'),
          // covidTest: Yup.string()
          //     .required('Covid tes is required'),
          // srf: Yup.string()
          //     .required('SRF is required'),
          // covidResult: Yup.string()
          //     .required('Covid Result is required'),
          // attendantName: Yup.string()
          //     .required('Attendant name is required'),
          // attendantMobile: Yup.string()
          //     .required('Attendant mobile is required'),
          // comorbidities: Yup.string()
          //     .required('Comorbidities is required'),
          // preferredHospital: Yup.string()
          //     .required('Preferred Hospital is required'),
          // bedTypes: Yup.string()
          //     .required('Bed types is required'),
          // others: Yup.string()
          //     .required('Others is required')
        })}
        onSubmit={(fields, { resetForm }) => {
          // setFields(fields)
          postPatient(fields, resetForm);
        }}
        render={({ errors, status, touched }) => (
          <Form>
            <Grid container spacing={6}>
              <Grid item sm={4} xs={12}>
                <div className="form-group">
                  <label htmlFor="firstName">First Name *</label>
                  <Field
                    name="firstName"
                    type="text"
                    className={
                      "form-control" +
                      (errors.firstName && touched.firstName
                        ? " is-invalid"
                        : "")
                    }
                  />
                  <ErrorMessage
                    name="firstName"
                    component="div"
                    className="invalid-feedback"
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="lastName">Last Name *</label>
                  <Field
                    name="lastName"
                    type="text"
                    className={
                      "form-control" +
                      (errors.lastName && touched.lastName ? " is-invalid" : "")
                    }
                  />
                  <ErrorMessage
                    name="lastName"
                    component="div"
                    className="invalid-feedback"
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="age">Age *</label>
                  <Field
                    name="age"
                    type="text"
                    className={
                      "form-control" +
                      (errors.age && touched.age ? " is-invalid" : "")
                    }
                  />
                  <ErrorMessage
                    name="age"
                    component="div"
                    className="invalid-feedback"
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="gender">Gender *</label>
                  <Field as="select" name="gender" className={"form-control"}>
                    <option value="male">Male</option>
                    <option value="female">Female</option>
                    <option value="other">Other</option>
                  </Field>

                  <ErrorMessage
                    name="gender"
                    component="div"
                    className="invalid-feedback"
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="adhaar">Patient Adhaar Number</label>
                  <Field
                    name="adhaar"
                    type="text"
                    className={
                      "form-control" +
                      (errors.adhaar && touched.adhaar ? " is-invalid" : "")
                    }
                  />
                  <ErrorMessage
                    name="adhaar"
                    component="div"
                    className="invalid-feedback"
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="lastName">Patient Mobile Number *</label>
                  <Field
                    name="mobile"
                    type="text"
                    className={
                      "form-control" +
                      (errors.mobile && touched.mobile ? " is-invalid" : "")
                    }
                  />
                  <ErrorMessage
                    name="mobile"
                    component="div"
                    className="invalid-feedback"
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="email">Email</label>
                  <Field
                    name="email"
                    type="text"
                    className={
                      "form-control" +
                      (errors.email && touched.email ? " is-invalid" : "")
                    }
                  />
                  <ErrorMessage
                    name="email"
                    component="div"
                    className="invalid-feedback"
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="address">Home Area / Address *</label>
                  <Field
                    name="address"
                    type="text"
                    className={
                      "form-control" +
                      (errors.address && touched.address ? " is-invalid" : "")
                    }
                  />
                  <ErrorMessage
                    name="address"
                    component="div"
                    className="invalid-feedback"
                  />
                </div>
              </Grid>
              <Grid item sm={4} xs={12}>
                <div className="form-group">
                  <label htmlFor="city">City</label>
                  <Field
                    name="city"
                    type="text"
                    className={
                      "form-control" +
                      (errors.city && touched.city ? " is-invalid" : "")
                    }
                  />
                  <ErrorMessage
                    name="city"
                    component="div"
                    className="invalid-feedback"
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="pincode">Pincode</label>
                  <Field
                    name="pincode"
                    type="text"
                    className={
                      "form-control" +
                      (errors.pincode && touched.pincode ? " is-invalid" : "")
                    }
                  />
                  <ErrorMessage
                    name="pincode"
                    component="div"
                    className="invalid-feedback"
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="symptoms">Symptoms</label>
                  <Field
                    name="symptoms"
                    type="text"
                    className={
                      "form-control" +
                      (errors.symptoms && touched.symptoms ? " is-invalid" : "")
                    }
                  />
                  <ErrorMessage
                    name="symptoms"
                    component="div"
                    className="invalid-feedback"
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="oxygen">SPO2 (Oxygen %)</label>
                  <Field
                    name="oxygen"
                    type="text"
                    className={
                      "form-control" +
                      (errors.oxygen && touched.oxygen ? " is-invalid" : "")
                    }
                  />
                  <ErrorMessage
                    name="oxygen"
                    component="div"
                    className="invalid-feedback"
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="patientIsIn">Patient is in</label>
                  <Field
                    as="select"
                    name="patientIsIn"
                    className={"form-control"}
                  >
                    <option value="home">Home</option>
                    <option value="out">Out</option>
                  </Field>
                  <ErrorMessage
                    name="patientIsIn"
                    component="div"
                    className="invalid-feedback"
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="hospital">Name of Hospital / PHC</label>
                  <Field
                    name="hospital"
                    type="text"
                    className={
                      "form-control" +
                      (errors.hospital && touched.hospital ? " is-invalid" : "")
                    }
                  />
                  <ErrorMessage
                    name="hospital"
                    component="div"
                    className="invalid-feedback"
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="ward">Ward Number / Name</label>
                  <Field
                    name="ward"
                    type="text"
                    className={
                      "form-control" +
                      (errors.ward && touched.ward ? " is-invalid" : "")
                    }
                  />
                  <ErrorMessage
                    name="ward"
                    component="div"
                    className="invalid-feedback"
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="covidTest">Covid Test</label>
                  <Field
                    as="select"
                    name="covidTest"
                    className={"form-control"}
                  >
                    <option value="rtpcr">RTPCR</option>
                    <option value="rapid">Rapid</option>
                  </Field>
                  <ErrorMessage
                    name="covidTest"
                    component="div"
                    className="invalid-feedback"
                  />
                </div>
              </Grid>
              <Grid item sm={4} xs={12}>
                <div className="form-group">
                  <label htmlFor="srf">SRF Number / BU Number</label>
                  <Field
                    name="srf"
                    type="text"
                    className={
                      "form-control" +
                      (errors.srf && touched.srf ? " is-invalid" : "")
                    }
                  />
                  <ErrorMessage
                    name="srf"
                    component="div"
                    className="invalid-feedback"
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="covidResult">COVID Result</label>
                  <Field
                    as="select"
                    name="covidResult"
                    className={"form-control"}
                  >
                    <option value="positive">Positive</option>
                    <option value="negative">Negative</option>
                  </Field>
                  <ErrorMessage
                    name="covidResult"
                    component="div"
                    className="invalid-feedback"
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="attendantName">Attendant Name</label>
                  <Field
                    name="attendantName"
                    type="text"
                    className={
                      "form-control" +
                      (errors.attendantName && touched.attendantName
                        ? " is-invalid"
                        : "")
                    }
                  />
                  <ErrorMessage
                    name="attendantName"
                    component="div"
                    className="invalid-feedback"
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="attendantMobile">
                    Attendant Mobile Number
                  </label>
                  <Field
                    name="attendantMobile"
                    type="text"
                    className={"form-control"}
                  />
                </div>

                <div className="form-group">
                  <label htmlFor="comorbidities">Comorbidities</label>
                  <Field
                    name="comorbidities"
                    type="text"
                    className={
                      "form-control" +
                      (errors.comorbidities && touched.comorbidities
                        ? " is-invalid"
                        : "")
                    }
                  />
                  <ErrorMessage
                    name="comorbidities"
                    component="div"
                    className="invalid-feedback"
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="preferredHospital">Prefeered Hospital</label>
                  <Field
                    as="select"
                    name="preferredHospital"
                    className={"form-control"}
                  >
                    <option value="gov_hosp">Govt</option>
                    <option value="pvt_hosp">Private</option>
                    <option value="care_center">Care Center</option>
                  </Field>
                  <ErrorMessage
                    name="preferredHospital"
                    component="div"
                    className="invalid-feedback"
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="bedTypes">Type of Bed</label>
                  <Field as="select" name="bedTypes" className={"form-control"}>
                    <option value="icu">ICU</option>
                    <option value="hdu">HDU</option>
                    <option value="ventilator">Ventilator</option>
                    <option value="general">General</option>
                  </Field>
                  <ErrorMessage
                    name="bedTypes"
                    component="div"
                    className="invalid-feedback"
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="others">Others</label>
                  <Field name="others" type="text" className={"form-control"} />
                </div>
              </Grid>
            </Grid>

            <div className="form-group">
              <button type="submit" className="btn btn-primary mr-2">
                Register
              </button>
              <button type="reset" className="btn btn-secondary">
                Reset
              </button>
            </div>
          </Form>
        )}
      />
    </div>
  );
};

export default AddPatient;
