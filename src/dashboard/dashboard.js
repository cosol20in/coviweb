import React, { useState } from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import Patientonboarding from '../patientOnboarding/patientOnboarding';
import KingBedIcon from '@material-ui/icons/KingBed';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import AssignmentTurnedInIcon from '@material-ui/icons/AssignmentTurnedIn';
import HelpIcon from '@material-ui/icons/Help';
import ContactPhoneIcon from '@material-ui/icons/ContactPhone';
import SettingsIcon from '@material-ui/icons/Settings';
import NotificationsIcon from '@material-ui/icons/Notifications';
import { Badge, Menu, MenuItem, MenuList } from '@material-ui/core';
import { AccountCircle, AddAlarm } from '@material-ui/icons';
import { Redirect, Route, Router, Switch, useHistory, withRouter } from 'react-router';
import PatientBedManagement from '../PatientBedManagement/PatientBedManagement';
import { NavLink } from 'react-router-dom';
import WildCardPage from '../components/WildCardPage'
import AddPatient from '../AddPatient/AddPatient';
import PatientDetails from '../PatientDetails/PatientDetails';


const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  toolbar: {
    flexGrow: 1
  },
  appBar: {
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,

  },
  drawerPaper: {
    width: drawerWidth,


  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
    textAlign: 'right'
  }
}));

const Dashboard = (props) => {
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);
  const history = useHistory();
  const [index, setIndex] = useState(0);

  
  const menuId = 'primary-search-account-menu';
  
 
  const [anchorEl, setAnchorEl] = React.useState(null);
  const openMenu = Boolean(anchorEl);
  const handleProfileMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const onClickButton = (index) => {
    setIndex(index);
    if (index === 0) {
      history.push('/')
    }
    if (index === 1) {
      history.push('/patient-bed-management')
    }

  }

  const links = [
    {
      name: 'Patient Onboarding',
      path: '/home',
      icon: <KingBedIcon color='primary' />,
      component: Patientonboarding
    },
    {
      name: 'Bed Management',
      path: '/home/patient-bed-management',
      icon: <AssignmentTurnedInIcon color='primary' />,
      component: Patientonboarding
    },
    {
      name: 'Add Patient',
      path: '/home/add-patient',
      icon: <AddCircleOutlineIcon color='primary' />,
      component: Patientonboarding
    },

  ]

  const activeRoute = (routeName) => {
    return props.location.pathname === routeName ? true : false;
  }

  const handleClose = (value) => {
    if(value === 'logout') {
      localStorage.clear();
      history.push('/')
    }
    setAnchorEl(null);
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
        <Toolbar className={classes.toolbar}>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, open && classes.hide)}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap style={{ flex: 'auto' }}>
            {index === 0 && 'CARE Patient Onboarding'}
            {index === 1 && 'CARE Bed Management'}
            {index === 2 && 'Add Patient'}
          </Typography>
          <div className={classes.sectionDesktop}>
            <IconButton aria-label="show 4 new mails" color="inherit" edge='end'>
              <Badge badgeContent={4} color="secondary">
                <MailIcon />
              </Badge>
            </IconButton>
            <IconButton aria-label="show 17 new notifications" color="inherit">
              <Badge badgeContent={17} color="secondary">
                <NotificationsIcon />
              </Badge>
            </IconButton>
            <IconButton
              edge="end"
              aria-label="account of current user"
              aria-controls={menuId}
              aria-haspopup="true"
              onClick={handleProfileMenuOpen}
              color="inherit"
            >
              <AccountCircle />
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorEl={anchorEl}
              anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              keepMounted
              transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              open={openMenu}
              onClose={handleClose}
            >
              <MenuItem onClick={handleClose}>Profile</MenuItem>
              <MenuItem onClick={() => handleClose('logout')}>Logout</MenuItem>
            </Menu>
          </div>

        </Toolbar>

      </AppBar>
      <Drawer
        className={classes.drawer}
        className={classes.drawer}
        variant="persistent"
        anchor="left"
        open={open}
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <div className={classes.drawerHeader}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
          </IconButton>
        </div>
        <Divider />
        {/* <List>
          {['Patient Onboarding', 'Bed Management', 'Add Patient'].map((text, index) => (
            <ListItem button key={text} onClick={() => { onClickButton(index) }}>
              <ListItemIcon>{index === 0 ? <KingBedIcon color='primary' /> : index === 1 ? <AssignmentTurnedInIcon color='primary' /> : <AddCircleOutlineIcon color='primary' />}</ListItemIcon>
              <ListItemText primary={text} />
            </ListItem>
          ))}
        </List> */}
        <MenuList>
          {links.map((value, index) => {
            return (
              <NavLink onClick={() => { onClickButton(index) }} to={value.path} key={index} style={{ textDecoration: 'none' }}>
                <MenuItem selected={activeRoute(value.path)}>
                  <ListItemIcon>{value.icon}</ListItemIcon>
                  <ListItemText primary={value.name} style={{ color: '#000' }} />
                </MenuItem>
              </NavLink>
            )

          })}
        </MenuList>
        <Divider />
        <List style={{ marginTop: '20%' }}>
          {['Supports', 'Help', 'Settings',].map((text, index) => (
            <ListItem button key={text}>
              <ListItemIcon>{index === 0 ? <ContactPhoneIcon color='primary' /> : index === 1 ? <HelpIcon color='primary' /> : <SettingsIcon color='primary' />}</ListItemIcon>
              <ListItemText primary={text} />
            </ListItem>
          ))}
        </List>
      </Drawer>

      <main
        className={clsx(classes.content, {
          [classes.contentShift]: open,
        })}
      >
        <div className={classes.drawerHeader} />

        <Switch>
          <Route exact path="/home">
            <Patientonboarding />
          </Route>
          <Route exact path="/home/patient-details">
            <PatientDetails />
          </Route>
          <Route path="/home/patient-bed-management">
            <PatientBedManagement />
          </Route>
          <Route path='/home/add-patient'>
            <AddPatient />
          </Route>
          <Route path='/home/404'>
            <WildCardPage />
          </Route>
          <Redirect to='/home/404'>

          </Redirect>
        </Switch>



      </main>
    </div>
  );
}


export default withRouter(Dashboard)