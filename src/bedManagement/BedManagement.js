import { useState, useEffect } from 'react';
import FavoriteIcon from '@material-ui/icons/Favorite';
import { Accordion, AccordionSummary, AccordionDetails, Grid, Radio, RadioGroup, FormControl, FormControlLabel, FormLabel } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { makeStyles } from '@material-ui/core/styles';
import _ from 'lodash'
import './BedManagement.scss';
import PatientRegistration from './PatientRegistration';
import { getServerBaseUrl } from '../common/utils';
import CareAPI from '../common/CareAPI';

const useStyles = makeStyles((theme) => ({
  header: {
    display: 'flex',
    borderBottom: '2px solid #E5E5E8',
    width: '95%',
    marginLeft: '2.5%',
  },
  headerLeft: {
    display: 'flex',
    alignItems: 'center',
    width: '80%',
  },
  headerLeftText: {
    color: '#153C87',
    display: 'block',
    fontWeight: 500,
    marginLeft: '10px'
  },
  headerRight: {
    borderLeft: '1px solid #EAEBED',
    textAlign: 'center',
  },
  headerRightHeading: {
    color: '#909090',
    fontWeight: 500,
    borderBottom: '1px solid #E5E5E8',
    marginBottom: '10px'
  },
  headerRightText: {
    fontSize: '14px',
    display: 'block',
    color: '#888888',
    marginBottom: '6px'

  }
}));


const BedManagement = () => {
  // States

  const classes = useStyles();

  const [activeFilters, setActiveFilters] = useState([]);

  const [organizations, setOrganizations] = useState([]);

  const [organizationsStatus, setOrganizationsStatus] = useState({});

  const [filteredOrgs, setFilteredOrgs] = useState([]);

  // Effects

  useEffect(() => {
    getOrganizations();
    getOrganizationsStatus();
  }, []);

  useEffect(() => {
    applyFilters();
  }, [activeFilters]);

  const getOrganizations = async () => {
    try {
      const res = await CareAPI.get('/api/orgs');
      if (res && res.status === 200) return setOrganizations(res.data.orgList);
    } catch(err) {
      console.log('error in fetching organizations', err);
    }
  };

  const getOrganizationsStatus = async () => {
    try {
      const res = await CareAPI.get('/api/orgs/status');
      if (res && res.status === 200) return setOrganizationsStatus(res.data);
    } catch (e) {
      console.log('Error fetching status', e);
    }
  }

  const toggleFilter = async (name) => {
/*    
    const array = [ ...activeFilters ];
    const index = array.indexOf(name);
    if (index === -1) {
      array.push(name);
    } else {
      array.splice(index, 1);
    }
    setActiveFilters(array)
*/

    try {
      let patientId = '60a0ae7b6e46c7174859d622';
      let orgId = '60a0ae676e46c7174859d5ff';
      let allocate = {
          "orgId": orgId,
          "type": "general",
          "ward": "12",
          "bedNo": "4"
      };

      const res = await CareAPI.put(`/api/patients/allocate/${patientId}`, allocate);

      console.log(`${res.status} -- ${res.statusText}`);
    } catch(err) {
      console.log('error in fetching organizations', err);
    }
  };


  const applyFilters = () => {
    const filteredOrgsArray = [];

    organizations.map((org) => {
      let passedAllFilters = true;

      activeFilters.map(filter => {
        const isFilterApplicable = checkFilter(org, filter);
        if (!isFilterApplicable) passedAllFilters = false;
      });

      if (passedAllFilters) filteredOrgsArray.push(org);

    });

    setFilteredOrgs(filteredOrgsArray);
  };

  const checkFilter = (org, filter) => {
    switch(filter) {
      case 'hdu':
        if (org.bedStatusInfo.hdu && org.bedStatusInfo.hdu > 0) return true
        else return false;
      case 'icu':
        if (org.bedStatusInfo.icu && org.bedStatusInfo.icu > 0) return true
        else return false;
      case 'ventilator':
        if (org.bedStatusInfo.ventilator && org.bedStatusInfo.ventilator > 0) return true
        else return false;
      case 'regular':
        if (org.bedStatusInfo.general && org.bedStatusInfo.general > 0) return true
        else return false;
      case 'pvt':
        if (org.orgType && org.orgType === 'pvt_hosp') return true
        else return false;
      case 'govt':
        if (org.orgType && org.orgType === 'govt_hosp') return true
        else return false;
      default:
        return false;
    }
  };

  // Render Functions

  const renderHeader = () => {
    return (
      <div className={classes.header} >
        <div className={classes.headerLeft} >
          <FavoriteIcon style={{fontSize: '56px', color: '#E5E5E8'}} />
          <div>
            <span className={classes.headerLeftText} style={{ marginBottom: '5px' }} >CARE</span>
            <span className={classes.headerLeftText} >Control room portal</span>
          </div>
        </div>
        <div>
          <span className={classes.headerRightHeading} >Overall Availability</span>
          <div className={classes.headerRight} >
            <span className={classes.headerRightText} >ICU: {organizationsStatus && organizationsStatus.availableBeds && organizationsStatus.availableBeds.icu ? organizationsStatus.availableBeds.icu : 0}</span>
            <span className={classes.headerRightText} >HDU: {organizationsStatus && organizationsStatus.availableBeds && organizationsStatus.availableBeds.hdu ? organizationsStatus.availableBeds.hdu : 0}</span>
            <span className={classes.headerRightText} >Ventilator: {organizationsStatus && organizationsStatus.availableBeds && organizationsStatus.availableBeds.ventilator ? organizationsStatus.availableBeds.ventilator : 0}</span>
            <span className={classes.headerRightText} >Regular: {organizationsStatus && organizationsStatus.availableBeds && organizationsStatus.availableBeds.general ? organizationsStatus.availableBeds.general : 0}</span>
          </div>
        </div>
      </div>
    );
  };

  const renderForm = () => {
    return (
      <div className='bedmanagement-form' >
        <div>

          <div className='bedmanagement-form-btns' >
            <button 
              className={`bedmanagement-form-btn ${activeFilters.indexOf('hdu') === -1 ? '' : 'active'}`}
              onClick={() => toggleFilter('hdu')}
            >
              HDU
            </button>

            <button 
              className={`bedmanagement-form-btn ${activeFilters.indexOf('icu') === -1 ? '' : 'active'}`}
              onClick={() => toggleFilter('icu')}
            >
              ICU
            </button>

            <button 
              className={`bedmanagement-form-btn ${activeFilters.indexOf('ventilator') === -1 ? '' : 'active'}`}
              onClick={() => toggleFilter('ventilator')}
            >
              Ventilator
            </button>

            <button 
              className={`bedmanagement-form-btn ${activeFilters.indexOf('regular') === -1 ? '' : 'active'}`}
              onClick={() => toggleFilter('regular')}
            >
              Regular
            </button>

            <button 
              className={`bedmanagement-form-btn ${activeFilters.indexOf('pvt') === -1 ? '' : 'active'}`}
              onClick={() => toggleFilter('pvt')}
            >
              Pvt
            </button>

            <button 
              className={`bedmanagement-form-btn ${activeFilters.indexOf('govt') === -1 ? '' : 'active'}`}
              onClick={() => toggleFilter('govt')}
            >
              Govt
            </button>

          </div>

          <div className='bedmanagement-form-location' >
            <label>Current location of patient</label>
            <input placeholder='Text Input' />
          </div>

          <button className='bedmanagement-form-submit' >Search Beds</button>

        </div>

        <div>
          <span className='bedmanagement-map-heading' >Current patient location</span>
          <img src='/map.png' />
          <span className='bedmanagement-map-text' >Locality: Jakkur</span>
          <span className='bedmanagement-map-text' >Ward: 49, Padmanabhapura</span>
        </div>

      </div>
    );
  };

  const HospitalDetails = ({ hospital }) => {
    return (
      <>
      <ul>
        <li>Name: {hospital.name}</li>
        <li>Address: {hospital.address ? `Street: ${hospital.address.street}, City: ${hospital.address.city}, Pincode: ${hospital.address.pincode}` : 'Not available'}</li>
        <li>Type: {hospital.orgType && hospital.orgType === 'pvt_hosp' ? 'Private' : 'Government'}</li>
        <li>Phone Number: <ul>{hospital.phones && hospital.phones.map((val, i) => (<li key={i}>{val}</li>))}</ul></li>
      </ul>
      <h3>Hospital Capacity Info</h3>
      <ul>
        <li>General: {hospital.bedCapInfo.general}</li>
        <li>HDU: {hospital.bedCapInfo.hdu}</li>
        <li>ICU: {hospital.bedCapInfo.icu}</li>
        <li>Ventilator: {hospital.bedCapInfo.ventilator}</li>
      </ul>
      <h3>Hospital Current Status Info</h3>
      <ul>
        <li>General: {hospital.bedStatusInfo.general}</li>
        <li>HDU: {hospital.bedStatusInfo.hdu}</li>
        <li>ICU: {hospital.bedStatusInfo.icu}</li>
        <li>Ventilator: {hospital.bedStatusInfo.ventilator}</li>
      </ul>
      </>
    )
  }

  const SelectHospitalBedType = ({ hospital, selectedType,  selectedHospital, openForm}) => {
    const handleChange = (e) => {
      selectedType(e.target.value);
      openForm(true);
      selectedHospital(hospital);
    }
    return (
      <FormControl component="fieldset">
        <FormLabel component="legend">Select Bed Type</FormLabel>
        <RadioGroup aria-label="bedType" name="bedType" onChange={handleChange}>
          <FormControlLabel value="general" disabled={hospital.bedStatusInfo.general && hospital.bedStatusInfo.general > 0 ? false : true } control={<Radio />} label="General" />
          <FormControlLabel value="hdu" disabled={hospital.bedStatusInfo.hdu && hospital.bedStatusInfo.hdu > 0 ? false : true } control={<Radio />} label="HDU" />
          <FormControlLabel value="icu" disabled={hospital.bedStatusInfo.icu && hospital.bedStatusInfo.icu > 0 ? false : true } control={<Radio />} label="ICU" />
          <FormControlLabel value="ventilator" disabled={hospital.bedStatusInfo.ventilator && hospital.bedStatusInfo.ventilator > 0 ? false : true } control={<Radio />} label="Ventilator" />
        </RadioGroup>
      </FormControl>
    )
  }

  const RenderResult = () => {
    const [hospital, setOrganization] = useState({});
    const [selectedType, setSelectedType] = useState(null);
    const [openForm, setOpenForm] = useState(false);
    return (
      <div>
        <span className='bedmanagement-result-heading' >Available beds</span>
        {activeFilters.length === 0 && organizations.map(organization => (
          <Accordion>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id={organization._id}
              style={{backgroundColor: '#CFD8DC', color: '#9DA6A9', fontSize: '18px', marginBottom: '5px' }}
            >
              <span>{`${organization.name}, 12km, HDU ${organization.bedStatusInfo.hdu}, ICU ${organization.bedStatusInfo.icu}, Ventilator ${organization.bedStatusInfo.ventilator}, Regular ${organization.bedStatusInfo.general}`}</span>
            </AccordionSummary>
            <AccordionDetails>
              <Grid container>
                <Grid item md={6}>
                  <HospitalDetails hospital={organization} />
                </Grid>
                <Grid item md={6}>
                  <SelectHospitalBedType hospital={organization} selectedType={setSelectedType} selectedHospital={setOrganization} openForm={setOpenForm} />
                </Grid>
              </Grid>
            </AccordionDetails>
          </Accordion>              
        ))}

        {activeFilters.length > 0 && filteredOrgs.map((organization, i) => (
          <Accordion  key={i}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id={organization._id}
            style={{backgroundColor: '#CFD8DC', color: '#9DA6A9', fontSize: '18px', marginBottom: '5px'}}
          >
            <span>{`${organization.name}, (${organization?.ward}), HDU ${organization.bedStatusInfo.hdu}, ICU ${organization.bedStatusInfo.icu}, Ventilator ${organization.bedStatusInfo.ventilator}, Regular ${organization.bedStatusInfo.general}`}</span>
          </AccordionSummary>
          <AccordionDetails>
            <Grid container>
              <Grid item md={6}>
                <HospitalDetails hospital={organization} />
              </Grid>
              <Grid item md={6}>
                <SelectHospitalBedType />
              </Grid>
            </Grid>
          </AccordionDetails>
        </Accordion>    
        ))}
      </div>
    );
  };

  return (
    <div>
      {renderHeader()}
      {renderForm()}
      <RenderResult />
      {/* <PatientRegistration organization={hospital} openForm={openForm} selectedType={selectedType} /> */}
    </div>
  )
}

export default BedManagement;