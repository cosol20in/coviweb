import React, { useState, useEffect } from 'react';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box';
import Link from '@material-ui/core/Link';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Slide from '@material-ui/core/Slide';
import { useHistory } from "react-router-dom";
import _ from 'lodash'
import { getServerBaseUrl } from './common/utils';
import CareAPI, { setClientToken } from './common/CareAPI';
import { Grid } from '@material-ui/core';
import covid from './assets/images/covid3.jpg'
import { Repeat } from '@material-ui/icons';

const loginSchema = Yup.object().shape({
  mobileno: Yup.string().required("Required")
});

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="#">
        CARE - Open Platform
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const Timer = ({ seconds }) => {
  const [counter, setCounter] = useState(seconds);

  useEffect(() => {
    const timer = counter > 0 && setInterval(() => setCounter(counter - 1), 1000);
    return () => clearInterval(timer);
  }, [counter]);

  return (
    <span>
      {counter}
    </span>
  )
}

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',

  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    color: '#FFFFFF !important'
  },
 
}));




const Login = () => {
  const history = useHistory();
  const classes = useStyles();
  const [enableOtpField, setEnableOtpField] = useState(false);

  const loginSubmit = async (values) => {
    if (!enableOtpField) {
      let response = await CareAPI.get('/api/users/getCode', { params: { phonenumber: values.mobileno } });
      if (response && response.status && response.status === 200) {
        setEnableOtpField(true);
      }
    } else {
      let response = await CareAPI.get('/api/users/verifyCode', { params: { phonenumber: values.mobileno, code: values.otp } });
      if (response && response.status && response.status === 200) {
        localStorage.setItem('access_token', response.data.access_token);
        localStorage.setItem('auth', true)
        setClientToken(response.data.access_token)
        history.push("/home");
        window.location.reload();
      }
    }
  }

  return (
    <Grid container spacing={2} >
      
      <Grid sm={8} xs={0}>
          <img src={covid} alt='image' style={{width: '98%', height: '98vh', overflow: 'hidden'}}/>
      </Grid>
      <Grid item xs={12} sm={4}>
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
      </Typography>
          <Formik
            initialValues={{ mobileno: '', otp: '' }}
            validationSchema={loginSchema}
            onSubmit={loginSubmit}
          >
            {({
              values,
              errors,
              touched,
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting,
              /* and other goodies */
            }) => (
              <form onSubmit={handleSubmit} className={classes.form} noValidate>
                <TextField
                  error={errors && errors.mobileno ? true : false}
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  id="mobileno"
                  label="Mobile no"
                  name="mobileno"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.mobileno}
                  helperText={errors.mobileno && touched.mobileno && errors.mobileno}
                  autoFocus
                />
                {enableOtpField ? (
                  <>
                    <Slide direction="left" in={enableOtpField} mountOnEnter unmountOnExit>
                      <TextField
                        error={errors && errors.otp ? true : false}
                        variant="outlined"
                        margin="normal"
                        fullWidth
                        id="otp"
                        label="OTP"
                        name="otp"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.otp}
                        helperText={errors.otp && touched.otp && errors.otp}
                        autoFocus
                      />

                    </Slide>
            Resend code in <Timer seconds={30} /> seconds
                  </>
                ) : null}
                <Button
                  type="submit"
                  disabled={isSubmitting}
                  fullWidth
                  variant="contained"
                  color="primary"
                  className={classes.submit}
                >
                  {enableOtpField ? 'Sign In' : 'Get OTP'}
                </Button>



                <Box mt={5}>
                  <center><a href="/about.html">About us</a></center>
                </Box>
                <Box mt={5}>
                  <Copyright />
                </Box>
              </form>
            )}
          </Formik>
        </div>
      </Grid>
    </Grid>

  )
}

export default Login;