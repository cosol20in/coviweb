import React, { useEffect, useState } from "react";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import {
  Button,
  Card,
  CardContent,
  CardHeader,
  Chip,
  FormControl,
  FormControlLabel,
  FormLabel,
  Grid,
  IconButton,
  InputLabel,
  MenuItem,
  Radio,
  RadioGroup,
  Select,
  Snackbar,
  TextField,
  Typography,
  Box
} from "@material-ui/core";
import Checkbox from "@material-ui/core/Checkbox";
import Input from "@material-ui/core/Input";
import ListItemText from "@material-ui/core/ListItemText";
import axios from "axios";
import map from "../assets/images/map.png";
import { getServerBaseUrl } from "../common/utils";
import CareAPI from "../common/CareAPI";
import ArrowDownwardIcon from "@material-ui/icons/ArrowDownward";
import { useHistory, useLocation } from "react-router";
import { ArrowBack } from "@material-ui/icons";
import Alert from "@material-ui/lab/Alert";
import { useSnackbar } from "notistack";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  card1: {
    background: "#f5f5f5",
  },
  card2: {
    background: "#f5f5f5",
    width: "100%",
  },
  textfiled: {
    minWidth: 340,
  },
  button: {
    height: "54px",
  },
  text1: {
    fontSize: "16px",
    paddingTop: "8px",
  },
  text2: {
    fontSize: "16px",
    fontWeight: "bold",
  },
  chip: {
    display: "flex",
    //justifyContent: "space-between",
    flexWrap: "wrap",
    marginRight: '20px',
    "& > *": {
      margin: theme.spacing(1.5),
      marginTop: theme.spacing(5),
      marginBottom: theme.spacing(5),
      
    },
  },
  grid: {
    paddingTop: theme.spacing(5),
    paddingLeft: theme.spacing(4),
  },
  beds: {
    width: "48px",
  },
}));

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const WardNames = ["All Wards", "My Wards", "Ward 04", "Ward 05"];

const PatientBedManagement = () => {
  const [age, setAge] = React.useState("");
  const [hospitals, setHospitals] = useState([]);
  const [beds, setBeds] = useState([]);
  const [data, setData] = useState(null);
  const [showbeds, setShowbeds] = useState(false);
  const [chipLabel, setChipLabel] = useState("general");
  const [value, setValue] = useState("general");
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  //const [patients, setPatients] = useLocation({});

  const history = useHistory();

  const location = useLocation();
  const patients = location?.state?.details?.data;
  const classes = useStyles();

  useEffect(() => {
    getHospitals();
    getAvailableBeds();
  }, []);

  const handleChange = (event) => {
    setAge(event.target.value);
  };

  const handleChangeBeds = (event) => {
    setValue(event.target.value);
  };

  const [open, setOpen] = useState(false);
  const [type, setType] = useState("");
  const [message, setMessage] = useState("");

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  const getHospitals = async () => {
    try {
      const res = await CareAPI.get("/api/orgs");
      if (res && res.status === 200) {
        setHospitals(res.data.orgList);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const getAvailableBeds = async () => {
    try {
      const res = await CareAPI.get("/api/orgs/status");
      if (res && res.status === 200) {
        setBeds(res.data.availableBeds);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const bookBeds = (res) => {
    if (res) {
      setData(res);
      setShowbeds(true);
    } else {
      setShowbeds(false);
    }
  };

  const onClickSave = async () => {
    const params = {
      orgId: data?._id,
      type: value,
    };
    try {
      const res = await CareAPI.put(
        `/api/patients/reserve/${patients._id}`,
        params
      );
      if (res && res.status === 200) {
        history.push("/home");
        enqueueSnackbar("Bed is assigned successfully", { variant: "success" });
      }
      if (res && res.status === 400) {
        enqueueSnackbar(res.message, { variant: "error" });
      }
    } catch (error) {
      console.log(error);
      enqueueSnackbar(error?.error?.message, { variant: "error" });
    }
  };

  const AssignBed = () => {
    return (
      <section>
        <Card className={classes.card1} style={{ marginTop: "78px" }}>
          <CardHeader subheader="Assign bed to:"></CardHeader>
          <CardContent>
            <Grid container>
              <Grid item sm={10} className={classes.text1}>
                Patient Name:{" "}
                <span className={classes.text2}>
                  {patients?.firstName} {patients?.lastName}
                </span>
              </Grid>
              <Grid item sm={2}>
                <FormControl variant="outlined" className={classes.formControl}>
                  <Select
                    labelId="demo-simple-select-outlined-label"
                    id="demo-simple-select-outlined"
                    value={10}
                    onChange={handleChange}
                  >
                    <MenuItem value={10}>Emergency</MenuItem>
                    <MenuItem value={20}>Crictical</MenuItem>
                    <MenuItem value={30}>Recovering</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
            </Grid>
            <br></br>
            <Grid container>
              <Grid sm={4} className={classes.text1}>
                Age: <span className={classes.text2}>{patients?.age}</span>
              </Grid>
              <Grid sm={4} className={classes.text1}>
                Location:{" "}
                <span className={classes.text2}>
                  {patients?.address?.street} {patients?.address?.city} -{" "}
                  {patients?.address?.pincode}
                </span>
              </Grid>
              <Grid
                sm={4}
                className={classes.text1}
                style={{ textAlign: "center" }}
              >
                Suggested:{" "}
                <span className={classes.text2}>
                  {patients?.moreInfo?.requested_bedtype
                    ? patients?.moreInfo?.requested_bedtype.toUpperCase()
                    : "-"}
                </span>
              </Grid>
            </Grid>
            <Grid container>
              <Grid item sm={10} className={classes.text1}>
                Gender:{" "}
                <span className={classes.text2}>{patients?.gender}</span>
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </section>
    );
  };

  const [indexes, setIndexes] = useState(1);

  const handleFilterChange = (index, label) => {
    setIndexes(index);
    const labels = label.toLowerCase();
    setValue(labels);

    const fil = () => {
      if (label === "HDU") {
        return hospitals.sort(
          (a, b) => b.availableBeds.hdu - a.availableBeds.hdu
        );
      } else if (label === "ICU") {
        return hospitals.sort(
          (a, b) => b.availableBeds.icu - a.availableBeds.icu
        );
      } else if (label === "Ventilator") {
        return hospitals.sort(
          (a, b) => b.availableBeds.ventilator - a.availableBeds.ventilator
        );
      } else if (label === "General") {
        return hospitals.sort(
          (a, b) => b.availableBeds.general - a.availableBeds.general
        );
      } else {
        return hospitals;
      }
    };

    setHospitals(fil);
  };

  const filterchip = [
    {
      id: 1,
      variant: "outlined",
      label: "All",
    },
    {
      id: 2,
      variant: "outlined",
      label: "HDU",
    },
    {
      id: 3,
      variant: "outlined",
      label: "ICU",
    },
    {
      id: 4,
      variant: "outlined",
      label: "Ventilator",
    },
    {
      id: 5,
      variant: "outlined",
      label: "General",
    },
  ];

  const hos = [
    {
      id: 6,
      variant: "outlined",
      label: "Private",
    },
    {
      id: 7,
      variant: "outlined",
      label: "Govt",
    },
  ];

  const ChipButton = () => {
    return (
      <section>
        <div className={classes.chip}>
          
            <Grid container direction="row">
              <Grid item direction="column" sm={8}>
                {filterchip.map((res, index) => (
                  <Chip
                    variant={indexes === res.id ? "default" : "outlined"}
                    size="large"
                    clickable
                    label={res.label}
                    onClick={() => handleFilterChange(res.id, res.label)}
                    color="primary"
                    style={{marginRight: '20px'}}
                     
                  />
                ))}
              </Grid>
              <Grid item direction="column" sm={4}>
                {hos.map((res) => (
                  <Chip
                    variant={indexes === res.id ? "default" : "outlined"}
                    size="large"
                    clickable
                    label={res.label}
                    onClick={() => handleFilterChange(res.id, res.label)}
                    color="secondary"
                    style={{marginRight: '20px'}}
                  />
                ))}
              </Grid>
            </Grid>
          
        </div>
      </section>
    );
  };

  const Hospitals = () => {
    return (
      <section>
        <Typography variant="subtitle1" style={{ marginBottom: "16px", marginLeft: '12px' }}>
          All availabe beds in - {selectedWardsText}
        </Typography>
        {!showbeds && (
          <Grid container>
            {hospitals.length > 0 &&
              hospitals.map((res, index) => (
                <>
                  <Grid item sm={4} xs={12}>
                    <Card style={{ margin: "8px" }} className={classes.card1}>
                      <CardContent>
                        <Typography>
                          <span style={{ fontWeight: "bold" }}>{res.name}</span>{" "}
                          ({res?.ward})
                        </Typography>
                        <br></br>
                        {(indexes && indexes === 1) ||
                        indexes === 6 ||
                        indexes === 7 ? (
                          <div>
                            <Grid container spacing={2}>
                              <Grid item sm={4} xs={6}>
                                <Typography variant="subtitle2">HDU</Typography>
                              </Grid>
                              <Grid item sm={4} xs={6}>
                                <Typography
                                  variant="subtitle2"
                                  style={{ fontWeight: "bold" }}
                                >
                                  {res?.availableBeds?.hdu}
                                </Typography>
                              </Grid>
                            </Grid>
                            <Grid container spacing={2}>
                              <Grid item sm={4} xs={6}>
                                <Typography variant="subtitle2">ICU</Typography>
                              </Grid>
                              <Grid item sm={4} xs={6}>
                                <Typography
                                  variant="subtitle2"
                                  style={{ fontWeight: "bold" }}
                                >
                                  {res?.availableBeds?.icu}
                                </Typography>
                              </Grid>
                            </Grid>

                            <Grid container spacing={2}>
                              <Grid item sm={4} xs={6}>
                                <Typography variant="subtitle2">
                                  Ventilator
                                </Typography>
                              </Grid>
                              <Grid item sm={4} xs={6}>
                                <Typography
                                  variant="subtitle2"
                                  style={{ fontWeight: "bold" }}
                                >
                                  {res?.availableBeds?.ventilator}
                                </Typography>
                              </Grid>
                            </Grid>
                            <Grid container spacing={2}>
                              <Grid item sm={4} xs={6}>
                                <Typography variant="subtitle2">
                                  General
                                </Typography>
                              </Grid>
                              <Grid item sm={4} xs={6}>
                                <Typography
                                  variant="subtitle2"
                                  style={{ fontWeight: "bold" }}
                                >
                                  {res?.availableBeds?.general}
                                </Typography>
                              </Grid>
                            </Grid>
                          </div>
                        ) : (
                          <div>
                            {indexes && indexes === 2 && (
                              <Grid container spacing={2}>
                                <Grid item sm={4} xs={6}>
                                  <Typography variant="subtitle2">
                                    HDU
                                  </Typography>
                                </Grid>
                                <Grid item sm={4} xs={6}>
                                  <Typography
                                    variant="subtitle2"
                                    style={{ fontWeight: "bold" }}
                                  >
                                    {res?.availableBeds?.hdu}
                                  </Typography>
                                </Grid>
                              </Grid>
                            )}
                            {indexes && indexes === 3 && (
                              <Grid container spacing={2}>
                                <Grid item sm={4} xs={6}>
                                  <Typography variant="subtitle2">
                                    ICU
                                  </Typography>
                                </Grid>
                                <Grid item sm={4} xs={6}>
                                  <Typography
                                    variant="subtitle2"
                                    style={{ fontWeight: "bold" }}
                                  >
                                    {res?.availableBeds?.icu}
                                  </Typography>
                                </Grid>
                              </Grid>
                            )}

                            {indexes && indexes === 4 && (
                              <Grid container spacing={2}>
                                <Grid item sm={4} xs={6}>
                                  <Typography variant="subtitle2">
                                    Ventilator
                                  </Typography>
                                </Grid>
                                <Grid item sm={4} xs={6}>
                                  <Typography
                                    variant="subtitle2"
                                    style={{ fontWeight: "bold" }}
                                  >
                                    {res?.availableBeds?.ventilator}
                                  </Typography>
                                </Grid>
                              </Grid>
                            )}
                            {indexes && indexes === 5 && (
                              <Grid container spacing={2}>
                                <Grid item sm={4} xs={6}>
                                  <Typography variant="subtitle2">
                                    General
                                  </Typography>
                                </Grid>
                                <Grid item sm={4} xs={6}>
                                  <Typography
                                    variant="subtitle2"
                                    style={{ fontWeight: "bold" }}
                                  >
                                    {res?.availableBeds?.general}
                                  </Typography>
                                </Grid>
                              </Grid>
                            )}
                          </div>
                        )}
                        <br></br>

                        <div
                          style={{ textAlign: "center", paddingTop: "12px" }}
                        >
                          <Button
                            variant="outlined"
                            size="small"
                            color="primary"
                            onClick={() => bookBeds(res)}
                          >
                            BOOK
                          </Button>
                        </div>
                      </CardContent>
                    </Card>
                  </Grid>
                </>
              ))}
          </Grid>
        )}
      </section>
    );
  };

  const BookBeds = () => {
    return (
      <section>
        {showbeds && data && (
          <Grid container>
            <Card className={classes.card2}>
              <IconButton
                style={{ paddingTop: "4px", paddingLeft: "8px" }}
                aria-label="delete"
                className={classes.margin}
                size="small"
                onClick={() => setShowbeds(false)}
              >
                <ArrowBack fontSize="inherit" /> Back
              </IconButton>

              <CardContent>
                <h6>
                  {data.name} <span> ({data?.ward})</span>
                </h6>

                {/* <h6>ICU:<span> {data?.availableBeds?.icu}</span></h6> */}

                <div>
                  <FormControl component="fieldset">
                    <FormLabel component="legend">
                      Confirm the choice of bed type
                    </FormLabel>
                    <RadioGroup
                      aria-label="gender"
                      name="gender1"
                      value={value}
                      onChange={handleChangeBeds}
                    >
                      <Grid container direction="row">
                        <Grid item direction="column" sm={6}>
                          <FormControlLabel
                            value="general"
                            control={<Radio />}
                            label="General"
                          />
                        </Grid>
                        <Grid
                          item
                          direction="column"
                          sm={6}
                          style={{ paddingTop: "10px" }}
                        >
                          ------- {data?.availableBeds?.hdu}
                        </Grid>
                      </Grid>
                      <Grid container direction="row">
                        <Grid item direction="column" sm={6}>
                          <FormControlLabel
                            value="icu"
                            control={<Radio />}
                            label="ICU"
                          />
                        </Grid>
                        <Grid
                          item
                          direction="column"
                          sm={6}
                          style={{ paddingTop: "10px" }}
                        >
                          ------- {data?.availableBeds?.icu}
                        </Grid>
                      </Grid>
                      <Grid container direction="row">
                        <Grid item direction="column" sm={6}>
                          <FormControlLabel
                            value="hdu"
                            control={<Radio />}
                            label="HDU"
                          />
                        </Grid>
                        <Grid
                          item
                          direction="column"
                          sm={6}
                          style={{ paddingTop: "10px" }}
                        >
                          ------- {data?.availableBeds?.hdu}
                        </Grid>
                      </Grid>
                      <Grid container direction="row">
                        <Grid item direction="column" sm={6}>
                          <FormControlLabel
                            value="ventilator"
                            control={<Radio />}
                            label="Ventilator"
                          />
                        </Grid>
                        <Grid
                          item
                          direction="column"
                          sm={6}
                          style={{ paddingTop: "10px" }}
                        >
                          ------- {data?.availableBeds?.ventilator}
                        </Grid>
                      </Grid>
                    </RadioGroup>
                  </FormControl>
                </div>
                <div>
                  <h6>
                    Book 1 {value.toUpperCase()} bed for {patients?.firstName}{" "}
                    {patients?.lastName}
                  </h6>
                </div>
                <div>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={onClickSave}
                  >
                    CONFIRM BED
                  </Button>
                </div>
              </CardContent>
            </Card>
          </Grid>
        )}
      </section>
    );
  };

  const [selWardNames, setSelWardNames] = React.useState(["All Wards"]);

  const [selectedWardsText, setSelectedWards] = React.useState(selWardNames);

  const handleWardChange = (event) => {
    setSelWardNames(event.target.value);

    let text = "";
    for (let i = 0, l = event.target.value.length; i < l; i += 1) {
      if (text.length > 0) {
        text += ", ";
      }
      text += event.target.value[i];
    }
    setSelectedWards(text);
  };

  return (
    <>
      <div className={classes.root}>
        <Grid container spacing={1}>
          <Grid item sm={4}>
            <Typography variant="h6">
              Welcome to the CARE control portal
            </Typography>
            <Typography variant="subtitle1">
              Search beds based on your preferred filters
            </Typography>
            <br />
            <Card className={classes.card1}>
              <CardHeader subheader="Overall Beds Availability"></CardHeader>
              <CardContent>
                <Grid container spacing={1}>
                  <Grid item sm={6}>
                    <Grid container spacing={1}>
                      <Grid item sm={6}>
                        {" "}
                        <p>ICU</p>
                      </Grid>
                      <Grid item sm={6}>
                        <h5>{beds?.icu}</h5>
                      </Grid>
                    </Grid>

                    <Grid container spacing={1}>
                      <Grid item sm={6}>
                        <p>HDU</p>
                      </Grid>
                      <Grid item sm={6}>
                        <h5>{beds?.hdu}</h5>
                      </Grid>
                    </Grid>
                  </Grid>

                  <Grid item sm={6}>
                    <Grid container spacing={1}>
                      <Grid item sm="6">
                        <p>Ventilator</p>
                      </Grid>
                      <Grid item sm="6">
                        <h5>{beds?.ventilator}</h5>
                      </Grid>
                    </Grid>

                    <Grid container spacing={1}>
                      <Grid item sm="6">
                        <p>General</p>
                      </Grid>
                      <Grid item sm="6">
                        <h5>{beds?.general}</h5>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </CardContent>
            </Card>
            <Typography style={{ marginTop: "32px" }}>
              Select the wards of preference
            </Typography>
            <Grid container spacing={2} style={{ marginTop: "16px" }}>
              <Grid item sm={9}>
                <FormControl variant="outlined" className={classes.formControl}>
                  <Select
                    labelId="demo-mutiple-checkbox-label"
                    id="demo-mutiple-checkbox"
                    multiple
                    value={selWardNames}
                    onChange={handleWardChange}
                    input={<Input />}
                    renderValue={(selected) => selected.join(", ")}
                    MenuProps={MenuProps}
                  >
                    {WardNames.map((name) => (
                      <MenuItem key={name} value={name}>
                        <Checkbox checked={selWardNames.indexOf(name) > -1} />
                        <ListItemText primary={name} />
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid>
            </Grid>
            <Grid container>
              <div>
                <img
                  src={map}
                  alt="map"
                  style={{
                    width: "100%",
                    height: "400px",
                    marginTop: "24px",
                    border: "1px solid black",
                  }}
                />
              </div>
            </Grid>
          </Grid>

          <Grid item sm={8} style={{ paddingLeft: "36px" }}>
            <AssignBed />
            <ChipButton />
            <Hospitals />
            <BookBeds />
          </Grid>
        </Grid>
      </div>
      <Snackbar open={open} autoHideDuration={5000} onClose={handleClose}>
        <Alert onClose={handleClose} severity={type} variant="filled">
          {message}
        </Alert>
      </Snackbar>
    </>
  );
};

export default PatientBedManagement;
