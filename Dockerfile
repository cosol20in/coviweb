# build environment
FROM node:12-alpine as build
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY package.json ./
COPY package-lock.json ./
COPY . ./
RUN npm install

RUN npm install react-scripts -g --silent
RUN npm audit fix
RUN npm run build

# release environment
FROM nginx:1.21
COPY --from=build /app/build /usr/share/nginx/html

# update config file
COPY nginx/nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]


# To build docker image
# docker build -f Dockerfile -t coviweb:release .

# To run a docker container
# docker run -it --rm -p 8000:80 coviweb:release
